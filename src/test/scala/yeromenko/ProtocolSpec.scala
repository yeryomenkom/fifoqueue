package yeromenko

import akka.util.ByteString
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers
import yeromenko.protocol.{Request, Response}

class ProtocolSpec extends AnyFreeSpec with Matchers {

  "Request" - {
    import Request.deserialize
    "should be deserialized successfully when data format is correct" in {
      deserialize(ByteString("PUT some line 123")) shouldBe Request.Put(ByteString("some line 123"))
      deserialize(ByteString("GET 12")) shouldBe Request.Get(12)
      deserialize(ByteString("QUIT")) shouldBe Request.Quit
      deserialize(ByteString("SHUTDOWN")) shouldBe Request.Shutdown
    }
    "should be deserialized as Unknown when data format is wrong" in {
      deserialize(ByteString("Put some line")) shouldBe Request.Unknown(ByteString(s"Put some line"))
      deserialize(ByteString("GET 0")) shouldBe Request.Unknown(ByteString("GET 0"))
      deserialize(ByteString("GET -1")) shouldBe Request.Unknown(ByteString("GET -1"))
      deserialize(ByteString("GET NotAnInteger")) shouldBe Request.Unknown(ByteString("GET NotAnInteger"))
      deserialize(ByteString("QUIT ")) shouldBe Request.Unknown(ByteString("QUIT "))
      deserialize(ByteString(" SHUTDOWN")) shouldBe Request.Unknown(ByteString(" SHUTDOWN"))
    }
  }

  "Response" - {
    import Response.serialize
    "should be serialized successfully" in {
      serialize(Response.Line(ByteString("some line 123"))) shouldBe ByteString("some line 123")
      serialize(Response.Error) shouldBe ByteString("ERR")
    }
  }

}
