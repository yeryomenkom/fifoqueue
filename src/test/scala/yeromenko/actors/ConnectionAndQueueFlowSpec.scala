package yeromenko.actors

import akka.actor.testkit.typed.scaladsl._
import akka.actor.typed.ActorRef
import akka.util.ByteString
import org.scalatest.BeforeAndAfterEach
import org.scalatest.freespec.AnyFreeSpecLike
import yeromenko.actors.ConnectionActor._
import yeromenko.protocol.{Request, Response}

class ConnectionAndQueueFlowSpec extends ScalaTestWithActorTestKit with AnyFreeSpecLike with BeforeAndAfterEach {
  private val testLines = Seq(1, 2, 3).map(ByteString(_))

  private var responseProbe1: TestProbe[Response] = _
  private var responseProbe2: TestProbe[Response] = _
  private var ackProbe: TestProbe[Unit] = _
  private var queue: ActorRef[QueueActor.Command] = _
  private var connection1: ActorRef[ConnectionActor.Command] = _
  private var connection2: ActorRef[ConnectionActor.Command] = _

  override protected def beforeEach(): Unit = {
    responseProbe1 = TestProbe[Response]
    responseProbe2 = TestProbe[Response]
    ackProbe = TestProbe[Unit]
    queue = testKit.spawn(QueueActor())
    connection1 = testKit.spawn(ConnectionActor(queue))
    connection2 = testKit.spawn(ConnectionActor(queue))
  }

  "ConnectionActor and QueueActor flow" - {
    "when single client" - {
      "should properly serve get request if it is enough lines in the queue" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ackProbe.ref ! ())
        ackProbe.expectMessage(())
        testLines.foreach { line =>
          connection1 ! HandleClientRequest(Request.Put(line))
          ackProbe.expectMessage(())
        }
        connection1 ! HandleClientRequest(Request.Get(testLines.size))
        ackProbe.expectMessage(())
        testLines.foreach { line =>
          responseProbe1.expectMessage(Response.Line(line))
          connection1 ! ClientAck
        }

        ackProbe.expectNoMessage()
        responseProbe1.expectNoMessage()
      }
      "should properly serve get request if it is not enough lines in the queue at the request time" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ackProbe.ref ! ())
        ackProbe.expectMessage(())
        testLines.foreach { line =>
          connection1 ! HandleClientRequest(Request.Put(line))
          ackProbe.expectMessage(())
        }
        connection1 ! HandleClientRequest(Request.Get(testLines.size + 1))
        ackProbe.expectMessage(())
        testLines.foreach { line =>
          responseProbe1.expectMessage(Response.Line(line))
          connection1 ! ClientAck
        }
        connection1 ! HandleClientRequest(Request.Put(testLines.head))
        ackProbe.expectMessage(())
        responseProbe1.expectMessage(Response.Line(testLines.head))

        ackProbe.expectNoMessage()
        responseProbe1.expectNoMessage()
      }
      "should stop itself if 'quit' request has been received" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ackProbe.ref ! ())
        ackProbe.expectMessage(())
        connection1 ! HandleClientRequest(Request.Quit)
        ackProbe.expectMessage(())
        responseProbe1.expectTerminated(connection1)

        ackProbe.expectNoMessage()
        responseProbe1.expectNoMessage()
      }
      "should terminate the queue if 'shutdown' request has been received" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ackProbe.ref ! ())
        ackProbe.expectMessage(())
        connection1 ! HandleClientRequest(Request.Shutdown)
        ackProbe.expectMessage(())
        responseProbe1.expectTerminated(queue)

        ackProbe.expectNoMessage()
        responseProbe1.expectNoMessage()
      }
      "should respond with error if invalid command has been received" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ackProbe.ref ! ())
        ackProbe.expectMessage(())
        connection1 ! HandleClientRequest(Request.Unknown(ByteString("invalid command")))
        ackProbe.expectMessage(())
        responseProbe1.expectMessage(Response.Error)

        ackProbe.expectNoMessage()
        responseProbe1.expectNoMessage()
      }
      "should order client responses properly (response message sequences should not interfere each other)" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ackProbe.ref ! ())
        ackProbe.expectMessage(())

        connection1 ! HandleClientRequest(Request.Put(testLines.head))
        ackProbe.expectMessage(())
        connection1 ! HandleClientRequest(Request.Get(2))
        ackProbe.expectMessage(())

        responseProbe1.expectMessage(Response.Line(testLines.head))
        connection1 ! ClientAck
        connection1 ! HandleClientRequest(Request.Unknown(ByteString("invalid command")))
        ackProbe.expectMessage(())
        connection1 ! HandleClientRequest(Request.Get(1))
        ackProbe.expectMessage(())

        responseProbe1.expectNoMessage()

        connection1 ! HandleClientRequest(Request.Put(testLines.head))
        ackProbe.expectMessage(())
        connection1 ! HandleClientRequest(Request.Put(testLines.head))
        ackProbe.expectMessage(())
        responseProbe1.expectMessage(Response.Line(testLines.head))
        connection1 ! ClientAck
        responseProbe1.expectMessage(Response.Error)
        connection1 ! ClientAck
        responseProbe1.expectMessage(Response.Line(testLines.head))
        connection1 ! ClientAck

        ackProbe.expectNoMessage()
        responseProbe1.expectNoMessage()
      }
    }
    "when multiple clients" - {
      "should return the queue elements evenly across the clients if they aren't slow" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ())
        connection2 ! ConnectionCreated(responseProbe2.ref ! _, () => ())

        testLines
          .flatMap(line => List(line, line))
          .foreach(line => connection1 ! HandleClientRequest(Request.Put(line)))

        connection1 ! HandleClientRequest(Request.Get(testLines.size * 2))
        connection2 ! HandleClientRequest(Request.Get(testLines.size * 2))

        testLines.foreach { line =>
          responseProbe1.expectMessage(Response.Line(line))
          connection1 ! ClientAck
          responseProbe2.expectMessage(Response.Line(line))
          connection2 ! ClientAck
        }
      }
      "should return the queue elements unevenly across the clients if one them is slow" in {
        connection1 ! ConnectionCreated(responseProbe1.ref ! _, () => ())
        connection2 ! ConnectionCreated(responseProbe2.ref ! _, () => ())

        testLines
          .flatMap(line => List(line, line))
          .foreach(line => connection1 ! HandleClientRequest(Request.Put(line)))

        connection1 ! HandleClientRequest(Request.Get(testLines.size * 2))
        connection2 ! HandleClientRequest(Request.Get(testLines.size * 2))

        responseProbe1.expectMessage(Response.Line(testLines.head))
        //don't do ack for client 1
        responseProbe2.expectMessage(Response.Line(testLines.head))
        connection2 ! ClientAck

        testLines.tail.foreach { line =>
          responseProbe2.expectMessage(Response.Line(line))
          connection2 ! ClientAck
          responseProbe2.expectMessage(Response.Line(line))
          connection2 ! ClientAck
        }
      }
    }
  }

}
