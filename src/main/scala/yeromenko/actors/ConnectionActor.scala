package yeromenko.actors

import akka.actor.typed.scaladsl.Behaviors._
import akka.actor.typed.scaladsl._
import akka.actor.typed.{ActorRef, Behavior}
import yeromenko.TcpServer
import yeromenko.protocol.{Request, Response}

import scala.collection.mutable

object ConnectionActor {
  sealed trait Command
  case class ConnectionCreated(send: Response => Unit, ack: () => Unit) extends Command
  case class ConnectionClosed(failure: Option[Throwable])               extends Command
  case class HandleClientRequest(request: Request)                      extends Command
  case class HandleQueueResponse(response: QueueActor.Response)         extends Command
  case object ClientAck                                                 extends Command

  def apply(queue: ActorRef[QueueActor.Command]): Behavior[Command] =
    receive {
      case (ctx, connection: ConnectionCreated) =>
        ctx.log.info(s"Connection created")
        connection.ack()
        new WorkingBehaviour(ctx, queue, connection)
      case _ => // I wanna be explicit and fail in case of receiving something unexpected
        unhandled
    }

  class WorkingBehaviour(
      ctx: ActorContext[Command],
      queue: ActorRef[QueueActor.Command],
      connection: ConnectionCreated
  ) extends AbstractBehavior(ctx) {

    sealed trait SendToClientEntry
    class Lines(var n: Int) extends SendToClientEntry
    case object Error       extends SendToClientEntry

    private val queueGetMsg     = QueueActor.Get(ctx.messageAdapter(HandleQueueResponse))
    private var canSendToClient = true
    private val sendToClient    = mutable.Queue.empty[SendToClientEntry]

    private def trySendToClient(): Unit =
      if (canSendToClient && sendToClient.nonEmpty) {
        sendToClient.front match {
          case lines: Lines =>
            queue ! queueGetMsg
            lines.n -= 1
            if (lines.n == 0) sendToClient.dequeue()
          case Error =>
            connection.send(Response.Error)
            sendToClient.dequeue()
        }
        canSendToClient = false
      }

    override def onMessage(msg: Command): Behavior[Command] = msg match {
      case HandleQueueResponse(QueueActor.GetResponse(value)) =>
        connection.send(Response.Line(value))
        same
      case HandleClientRequest(req) =>
        connection.ack()
        req match {
          case Request.Get(n) =>
            sendToClient.enqueue(new Lines(n))
            trySendToClient()
            this
          case Request.Put(line) =>
            queue ! QueueActor.Put(line)
            this
          case Request.Quit =>
            stopped
          case Request.Shutdown =>
            queue ! QueueActor.Shutdown
            this
          case Request.Unknown(raw) =>
            ctx.log.warn(s"Received unknown client request: ${raw.utf8String}")
            sendToClient.enqueue(Error)
            trySendToClient()
            this
        }
      case ConnectionClosed(failure) =>
        ctx.log.info(s"Connection closed", failure.orNull)
        stopped
      case ClientAck =>
        canSendToClient = true
        trySendToClient()
        this
      case _ =>
        unhandled
    }
  }

  def adaptToTcp(behavior: Behavior[Command]): Behavior[TcpServer.ConnectionEvent] = {
    import TcpServer.ConnectionEvent._
    behavior.transformMessages[TcpServer.ConnectionEvent] {
      case Received(msg) =>
        HandleClientRequest(Request.deserialize(msg))
      case Created(connection) =>
        ConnectionCreated(resp => connection.send(Response.serialize(resp)), connection.ack)
      case Closed(failure) =>
        ConnectionClosed(failure)
      case Ack =>
        ClientAck
    }
  }
}
