package yeromenko.actors

import akka.actor.typed.scaladsl.Behaviors._
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.ByteString

import scala.collection.immutable.Queue

object QueueActor {
  sealed trait Command
  case class Get(replyTo: ActorRef[GetResponse]) extends Command
  case class Put(value: ByteString)              extends Command
  case object Shutdown                           extends Command

  sealed trait Response
  case class GetResponse(value: ByteString) extends Response

  def apply(): Behavior[Command] =
    working(Queue.empty, Queue.empty)

  def working(values: Queue[ByteString], waitingForReply: Queue[ActorRef[GetResponse]]): Behavior[Command] =
    receiveMessage {
      case Get(replyTo) if values.nonEmpty =>
        val (value, updatedValues) = values.dequeue
        replyTo ! GetResponse(value)
        working(updatedValues, waitingForReply)
      case Get(replyTo) =>
        working(values, waitingForReply.enqueue(replyTo))
      case Put(value) if waitingForReply.nonEmpty =>
        val (replyTo, updatedWaitingForReply) = waitingForReply.dequeue
        replyTo ! GetResponse(value)
        working(values, updatedWaitingForReply)
      case Put(value) =>
        working(values.enqueue(value), waitingForReply)
      case Shutdown =>
        stopped
    }
}
