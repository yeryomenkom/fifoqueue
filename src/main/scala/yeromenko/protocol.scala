package yeromenko

import akka.util.ByteString

object protocol {

  sealed trait Request
  object Request {
    case class Put(line: ByteString) extends Request
    case class Get(numberOfLines: Int) extends Request
    case object Quit extends Request
    case object Shutdown extends Request
    case class Unknown(raw: ByteString) extends Request

    val deserialize: ByteString => Request = {
      val prefixPut = ByteString("PUT ")
      val prefixGet = ByteString("GET ")
      val quit = ByteString("QUIT")
      val shutdown = ByteString("SHUTDOWN")

      raw =>
        if (raw.startsWith(prefixPut))
          Put(raw.drop(prefixPut.size))
        else if (raw.startsWith(prefixGet))
          raw.drop(prefixGet.size).utf8String.toIntOption.filter(_ > 0).map(Get).getOrElse(Unknown(raw))
        else if (raw == quit)
          Quit
        else if (raw == shutdown)
          Shutdown
        else
          Unknown(raw)
    }
  }

  sealed trait Response
  object Response {
    case class Line(line: ByteString) extends Response
    case object Error extends Response

    val serialize: Response => ByteString = {
      case Line(line) => line
      case Error => ByteString("ERR")
    }
  }

}
