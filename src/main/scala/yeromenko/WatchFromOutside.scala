package yeromenko

import akka.actor.ActorSystem
import akka.actor.typed._
import akka.actor.typed.scaladsl.Behaviors._
import akka.actor.typed.scaladsl.adapter._

import scala.concurrent.ExecutionContext

object WatchFromOutside {
  private case object ActorHasStopped

  def apply[T](actor: ActorRef[T])(handleStop: => Unit)
              (implicit system: ActorSystem, ec: ExecutionContext): ActorRef[Nothing] = {
    val behaviour: Behavior[Nothing] = setup [ActorHasStopped.type] { ctx =>
      ctx.watchWith(actor, ActorHasStopped)
      receiveMessage {
        case ActorHasStopped =>
          ec.execute(() => handleStop)
          stopped
      }
    }.narrow
    system.toTyped.systemActorOf[Nothing](behaviour, s"watch_from_outside_${actor.path.name}")
  }
}
