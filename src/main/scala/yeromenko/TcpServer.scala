package yeromenko

import java.net.InetSocketAddress

import akka.Done
import akka.actor.ActorSystem
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.adapter._
import akka.stream.scaladsl.Tcp.IncomingConnection
import akka.stream.scaladsl._
import akka.stream.typed.scaladsl.{ActorSink, ActorSource}
import akka.util.ByteString
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.Future

object TcpServer extends StrictLogging {
  trait Connection {
    /**
     * Make sure that you received [[TcpServer.ConnectionEvent.Ack]] for the previous sent frame
     * before sending the next one. Otherwise connection will fail.
     */
    def send(frame: ByteString): Unit
    def ack(): Unit
  }

  sealed trait ConnectionEvent
  object ConnectionEvent {
    case class Created(connection: Connection) extends ConnectionEvent
    case class Closed(ex: Option[Throwable])   extends ConnectionEvent
    case class Received(frame: ByteString)     extends ConnectionEvent
    case object Ack                            extends ConnectionEvent
  }

  private def createConnectionActorName(clientIP: InetSocketAddress): String = {
    val remote =
      if (clientIP.isUnresolved) s"unknown_${System.nanoTime}"
      else s"${clientIP.getAddress.getHostAddress}:${clientIP.getPort}"

    s"tcp_connection_$remote"
  }

  /**
   * Starts tcp server based on akka streams with back pressure feature.
   * @param connectionBehaviour behaviour for an actor which will be used to handle each incoming connection.
   *                            By contract, [[TcpServer.ConnectionEvent.Created]] message will be once sent to the actor before any other messages.
   *                            This message should be acknowledged as well as all [[TcpServer.ConnectionEvent.Received]] messages.
   *                            Connection will be closed in case if actor will stop.
   */
  def start(
      port: Int,
      connectionBehaviour: Behavior[ConnectionEvent],
      frameDelimiter: String = "\r\n",
      maximumFrameLength: Int = Int.MaxValue
  )(
      implicit
      system: ActorSystem
  ): Future[Tcp.ServerBinding] = {
    val delimiter = ByteString(frameDelimiter)
    val handleConnections: Sink[IncomingConnection, Future[Done]] = Sink.foreach { incoming =>
      val connectionActor = system.toTyped
        .systemActorOf(connectionBehaviour, createConnectionActorName(incoming.remoteAddress))

      val (frameOut, source) = ActorSource
        .actorRefWithBackpressure[ByteString, ConnectionEvent.Ack.type](
          ackTo = connectionActor,
          ackMessage = ConnectionEvent.Ack,
          completionMatcher = PartialFunction.empty,
          failureMatcher = PartialFunction.empty
        )
        .map { _ ++ delimiter }
        .preMaterialize()

      val sink = ActorSink
        .actorRefWithBackpressure[ByteString, ConnectionEvent, Unit](
          connectionActor,
          messageAdapter = (_, frame) => ConnectionEvent.Received(frame),
          onInitMessage = ackOut =>
            ConnectionEvent.Created(new Connection {
              override def send(frame: ByteString): Unit = frameOut ! frame
              override def ack(): Unit                   = ackOut ! ()
            }),
          ackMessage = (),
          onCompleteMessage = ConnectionEvent.Closed(None),
          onFailureMessage = (ex: Throwable) => ConnectionEvent.Closed(Some(ex))
        )

      val connectionFlow = Framing
        .delimiter(delimiter, maximumFrameLength = maximumFrameLength)
        .via(Flow.fromSinkAndSourceCoupled(sink, source))

      incoming.handleWith(connectionFlow)
    }

    Tcp().bind("0.0.0.0", port).to(handleConnections).run()
  }

}
