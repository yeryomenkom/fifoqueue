package yeromenko

import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import com.typesafe.scalalogging.StrictLogging
import yeromenko.actors.{ConnectionActor, QueueActor}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object Main extends App with StrictLogging {
  implicit val system: ActorSystem = ActorSystem("queue_service")

  val queue = system.toTyped.systemActorOf(QueueActor(), "queue")
  val tcpBinding = TcpServer.start(
    port = 10042,
    connectionBehaviour = ConnectionActor.adaptToTcp(ConnectionActor(queue)),
    frameDelimiter = "\r\n"
  )

  tcpBinding.onComplete {
    case Failure(ex) =>
      logger.warn(s"Tcp server couldn't start. App will shutdown.", ex)
      system.terminate()
    case Success(binding) =>
      logger.info(s"Tcp server started. ${binding.localAddress}.")
      binding.whenUnbound.onComplete {
        case Failure(ex) =>
          logger.warn(s"Tcp server failed. App will shutdown.", ex)
          system.terminate()
        case Success(_) =>
          logger.info("Tcp server stopped.")
      }
  }
  WatchFromOutside(queue) {
    logger.info(s"Queue actor stopped. App will shutdown.")
    tcpBinding.flatMap(_.unbind).onComplete(_ => system.terminate())
  }
}
