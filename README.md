### FIFO Queue

#### How does your system work?

I decided to focus on handling multiple concurrent clients. My solution is completely in-memory and
provides "at-most-once" delivery semantic. So, at first, I want to provide a few reasons why I have picked this, 
instead of making the queue persistent. The proper implementation of such persistence would require work with
some kind of segmented append only log and offsets (something like kafka or Aeron IPC does). So, I wasn't sure
I would be able to finish this code challenge in time (my goal was to spent not more than 1 working day). 
Another reason is that even if I would implement something like that, I still wouldn't be able to achieve 
more desirable delivery semantics (e.g. "at-least-once"). For being able to achieve that I would need to add
some kind of acknowledgments to the consumer protocol or maybe even move the offset committing to the consumer side.
So, in the end I would find myself implementing another kafka, which is strange.

The main feature of my solution is back pressure. So, even though we have "at-most-once" delivery guarantee,
the app hopefully will lose as few data as possible. In case if some consumer becomes slow (meaning it can't
consume a requested amount of values fast enough), the app will react to this and stop sending data to it until consumer
will be able to catch up. Such approach minimizes the possibility of data loss due to tcp buffer overflowing and
improves the overall throughput by sending data to others healthy consumers.

Few points about the codebase itself.
1. When I am dealing with some kind of external protocol, I always try to define families of ADTs which describe it.
So, all my domain abstractions work only with these ADTs. It allows me to switch from one serialization format to 
another one or support multiple at the same time.
2. TcpServer is self-contained and decoupled from the rest of the app. ConnectionActor also doesn't depend on any
TcpServer abstractions. The only bridge between these two is "adaptToTcp" pure function. 
Such design allows to easily switch from tcp to web socket for example, or even use multiple transports 
at the same time without any adjustments from the ConnectionActor side.
3. I didn't have enough time for introducing full test coverage. Though, I implemented a few the most important tests from 
my point of view. The first one is ProtocolSpec which ensures that we can properly serialize/deserialize protocol's ADTs.
This is an example of unit test. I am not the biggest fan of unit testing every single line of code, instead 
I lean more towards contract (or end-to-end) tests. However, in case of ProtocolSpec, it is actually something that
fixes the contract (contract of protocol messages format), and it can serve as a part of the high level documentation.
So, I like this unit test. Ideal test for me is when we pack our app (nowadays it is docker image I would say),
run it just like we would do it in prod and run some test cases (actually it is exactly my setup on the current work. 
thanks to testcontainers lib). Unfortunately such tests require certain infrastructure, so I decided not to 
overcomplicate things and used just akka testkit to test more or less full flow of the app in ConnectionAndQueueFlowSpec.
Even though it serves the current needs fine, the point has to be made that in case we decide to switch to netty
instead of akka in the future, we have to throw away this test, despite that our contract for the app didn't change.

#### How will your system perform as the number of requests per second increases?

It has to be measured of course, but I think akka is capable of serving hundreds of thousands of requests per second.

#### How will your system perform with various queue sizes?

If the queue size grows big enough, my app fails with OOM exception. I could implement some limiting queue strategy,
but all of them are quite specific and should be picked according to the particular use case (e.g. "drop-tail", 
"drop-head", "drop-all"). To be honest I think that all of them are kind of hot fix and do not solve the real problem.
The proper solution from my point of view would be to introduce more consumers or switch to some disk persistent queue.
Another interesting option which I considered is to use back pressure. Meaning we stop consuming PUT requests from 
the client until queue will have enough capacity. Unfortunately, since each client can do PUT and GET requests, we may 
end up in the situation when all clients are just stack on PUT command. Such approach would work if we split our
clients into two categories: producers and consumers.

#### What documentation, websites, papers, etc did you consult in doing this assignment?

Hard to say... I would say nothing, but I can mention things which I read/watched before and which I think
helped me to complete this challenge:
1. Designing Data-Intensive Applications by Martin Kleppmann (book)
2. A lot of akka docs and talks, especially regarding the back pressure
3. Adventures with concurrent programming in Java: A quest for predictable latency by Martin Thompson (talk, 
Aeron IPC was mentioned there and in general a lot of interesting stuff about queues in java)

#### What third-party libraries or other tools does the system use?

akka typed, akka streams typed, scala logging, logback, scala test, akka testkit

#### How long did you spend on this exercise?

Coding took 1 working day more or less, but I had time to think about the task before coding. 
So, it would be fair to say that it took me up to 2 working days.