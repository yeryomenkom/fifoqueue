name := "FifoQueue"
version := "0.1"
scalaVersion := "2.13.3"

val akka = "2.6.8"
libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-actor-typed" % akka,
  "com.typesafe.akka" %% "akka-stream-typed" % akka,

  "org.scalatest" %% "scalatest" % "3.1.1" % Test,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akka % Test
)

assemblyJarName in assembly := "FifoQueue.jar"
